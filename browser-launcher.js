const puppeteer = require('puppeteer');

async function browserLauncher(options) {
    return await puppeteer.launch({
        headless: options.headless,
        defaultViewport: {
            width: options.width,
            height: options.height
        }
    });
}

module.exports = browserLauncher;
