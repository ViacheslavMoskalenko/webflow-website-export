const BasePage = require('./base-page');
const {TIMEOUT, FS_POLLING_INTERVAL, SHORT_MEANINGFUL_TIMEOUT} = require('./config');
const fs = require('fs');
const path = require('path');

class ProjectPage extends BasePage {
    constructor(browser, projectId) {
        super(browser);
        this._projectId = projectId;
    }

    async open(options) {
        await super.open(`https://webflow.com/design/${this._projectId}`, options);
    }

    async exportProjectAsZip(saveToFolder) {
        await this._openExportDialog();
        await this._prepareArtifact();
        await this._startDownloadingArtifact();
        await this._verifyArtifactIsDownloaded(saveToFolder);
    }

    async _getArtifactName() {
        return await this._page.evaluate(() => {
            const link = document.querySelector('a[href^="blob:"]');
            const projectId = link.href.slice(link.href.lastIndexOf('/') + 1);
            const generatedArtifactName = `${projectId}.zip`;
            const customArtifactName = link.getAttribute('download');
            return customArtifactName || generatedArtifactName;
        });
    }

    async _verifyArtifactIsDownloaded() {
        if (!this._downloadPath) {
            throw new Error('Unable to verify if file is downloaded. Download path is not set');
        }
        const artifactName = await this._getArtifactName();
        const artifactPath = path.join(this._downloadPath, artifactName);
        await new Promise(res => {
            (function checkFile() {
                if (fs.existsSync(artifactPath)) {
                    console.log(`File ${artifactName} has been successfully downloaded`);
                    return res();
                }
                console.log(`File is not downloaded yet. Waiting ${FS_POLLING_INTERVAL} ms...`);
                setTimeout(checkFile, FS_POLLING_INTERVAL);
            })();
        });
    }

    async _startDownloadingArtifact() {
        const downloadZipBtnSelector = '//*[text()="Download ZIP"]/parent::div/parent::a';
        await this._page.waitForXPath(downloadZipBtnSelector, {timeout: TIMEOUT});
        const downloadZipBtnResults = await this._page.$x(downloadZipBtnSelector);
        const downloadZipBtn = downloadZipBtnResults[0];
        await downloadZipBtn.click();
        console.log('Clicked "download zip" button');
    }

    async _prepareArtifact(attempts = 3) {
        if (attempts <= 0) throw new Error('Unable to prepare artifact');
        const prepareExportBtnSelector = '//*[text()="Prepare ZIP"]/parent::*';
        try {
            await this._page.waitForXPath(prepareExportBtnSelector, {timeout: TIMEOUT});
            const prepareZipBtnResults = await this._page.$x(prepareExportBtnSelector);
            const prepareZipBtn = prepareZipBtnResults[0];
            await prepareZipBtn.click();
            console.log('Clicked "prepare zip" button');
        } catch (error) {
            const closeDialogSelector = '[data-prevent-global-event-handlers]';
            await this._page.waitForSelector(closeDialogSelector);
            await this._page.click(closeDialogSelector);
            console.log('Unable to find "prepare zip" button. Retrying...');
            await this._prepareArtifact(--attempts);
        }
    }

    async _openExportDialog(attempts = 3) {
        if (attempts <= 0) throw new Error('Unable to invoke export dialog');
        const exportBtnSelector = '.bem-TopBar_Body_Button.bem-TopBar_Body_ExportButton';
        await this._page.waitForSelector(exportBtnSelector);
        await this._page.click(exportBtnSelector);
        console.log('Clicked on export button');
        try {
            await this._page.waitForXPath('//*[text()="Export Code"]', {timeout: SHORT_MEANINGFUL_TIMEOUT});
            console.log('Code export dialog appeared');
        } catch (err) {
            console.log('Code export dialog did not appear. Retrying...');
            await this._openExportDialog(--attempts);
        }
    }
}

module.exports = ProjectPage;
