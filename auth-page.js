const BasePage = require('./base-page');
const {DELAY, TIMEOUT} = require('./config');

class AuthPage extends BasePage {
    constructor(browser) {
        super(browser);
    }

    async open(options) {
        await super.open('https://webflow.com/dashboard/login', options);
    }

    async authenticate(userName, password) {
        await this._enterUserName(userName);
        await this._enterPassword(password);
        await this._submitForm();
        console.log('Successfully authenticated a user');
    }

    async _enterUserName(userName) {
        const userNameFieldSelector = 'input[data-automation-id="username-input"]';
        await this._page.waitForSelector(userNameFieldSelector);
        await this._page.type(userNameFieldSelector, userName, {delay: DELAY});
        console.log('Entered username');
    }

    async _enterPassword(password) {
        const passwordFieldSelector = 'input[data-automation-id="password-input"]';
        await this._page.waitForSelector(passwordFieldSelector);
        await this._page.type(passwordFieldSelector, password, {delay: DELAY});
        console.log('Entered password');
    }

    async _submitForm() {
        const authBtnSelector = 'button[data-automation-id="login-button"]';
        await this._page.click(authBtnSelector);
        await this._page.waitForSelector('[data-automation-id="new-site-button"]', {timeout: TIMEOUT});
        console.log('Clicked auth button');
    }
}

module.exports = AuthPage;
