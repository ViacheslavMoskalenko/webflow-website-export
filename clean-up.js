const fs = require('fs');
const ARTIFACT_TRAITS_RE = /\.zip$/i;

function cleanUp() {
    fs.readdirSync(__dirname).forEach(file => {
        if(ARTIFACT_TRAITS_RE.test(file)) fs.unlinkSync(file);
    });
}

module.exports = cleanUp;
