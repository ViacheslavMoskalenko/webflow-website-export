const {TIMEOUT} = require('./config');

class BasePage {
    constructor(browser) {
        if (this.constructor === BasePage) throw new Error('BasePage class is abstract');
        this._client = null;
        this._page = null;
        this._browser = browser;
        this._downloadPath = '';
    }

    async open(url, options = {}) {
        if (this._page) throw new Error('Unable to navigate away from already opened page');
        this._page = await this._browser.newPage();
        this._client = await this._page.target().createCDPSession();
        await this._page.goto(url, Object.assign({}, options, {
            timeout: TIMEOUT
        }));
        console.log(`Navigated to ${url}`);
    }

    async enableAutomaticFilesDownload(path) {
        if (!this._client) throw new Error('Page client does not exist');
        this._downloadPath = path;
        await this._client.send('Page.setDownloadBehavior', {
            behavior: 'allow',
            downloadPath: path
        });
        console.log(`Browser is configured to automatically download files to ${path}`);
    }

    async close() {
        await this._page.close();
        this._page = null;
        this._client = null;
    }
}

module.exports = BasePage;
