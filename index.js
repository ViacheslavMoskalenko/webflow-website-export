const cleanUp = require('./clean-up');
const browserLauncher = require('./browser-launcher');
const AuthPage = require('./auth-page');
const ProjectPage = require('./project-page');
const {
    WF_USER: userName,
    WF_PASS: password,
    MODE: mode,
    PROJECT: project
} = process.env;

(async function exportWebsite() {
    let browser;
    try {
        cleanUp();
        browser = await browserLauncher({
            headless: mode === 'headless',
            width: 1366,
            height: 768
        });

        const authPage = new AuthPage(browser);
        await authPage.open({
            waitUntil: 'networkidle2'
        });
        await authPage.authenticate(userName, password);
        await authPage.close();


        const projectPage = new ProjectPage(browser, project);
        await projectPage.open({
            waitUntil: 'networkidle2'
        });
        await projectPage.enableAutomaticFilesDownload(__dirname);
        await projectPage.exportProjectAsZip();
        await projectPage.close();
    } catch (err) {
        console.error('Unable to finish the export job: ', err.message);
        process.exit(1);
    } finally {
        console.log('Closing the browser');
        await browser.close();
    }
})();
