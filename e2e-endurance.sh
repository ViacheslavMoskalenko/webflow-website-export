#!/usr/bin/env bash
echo "Initiating endurance test. Please, be patient..."
counter=0
for ((; counter < 10; counter++))
do
    npm run start:headless
    echo "${counter} iterations made"
done
echo "Done"
